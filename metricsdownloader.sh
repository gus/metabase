#!/bin/bash
#
# Simple metrics.torproject.org downloader.
# $ apt install curl sqlite3
echo "Downloading data sets..."

curl -O https://metrics.torproject.org/userstats-relay-country.csv
curl -O https://metrics.torproject.org/userstats-bridge-country.csv
#curl -O https://metrics.torproject.org/userstats-bridge-combined.csv

echo "Removing top 5 lines..."
sed -i '1,5d' userstats-relay-country.csv
sed -i '1,5d' userstats-bridge-country.csv
#sed -i '1,5d' userstats-bridge-combined.csv

# remove old dbs
rm userstats-relay-country.db
rm bridge_country.db
#rm bridge_combined.db

echo "Import db userstats-relay-country"
(echo .separator ,; echo .import userstats-relay-country.csv users_stats) | sqlite3 userstats-relay-country.db

echo "Import db bridge_country"
(echo .separator ,; echo .import userstats-bridge-country.csv bridge_country) | sqlite3 bridge_country.db

#(echo .separator ,; echo .import userstats-bridge-combined.csv bridge_combined) | sqlite3 bridge_combined.db
