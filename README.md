## Metrics data Viz

Notes & Step by step:

 1. Download Metrics data sets: Tor users, Bridge users, Bridge transports.
 2. Remove CSV extra lines.
 3. Import CSV to SQLite.
 4. Connect Metabase and SQLite.
 5. Metabase data model: select date as a date format, users field as a number, Country field as Country.
 6. Create metrics: country + SUM users.
 7. Create dashboard and add questions.
 8. On Dashboard, create Filters, for example, "date" and you pick all the field "date" of the queries.
 9. Important: to sum all users, you must filter country by "IS NOT" empty/blank, otherwise if you query the whole table, you will get an empty country with the total per day.
 10. Please validate the data viz on https://metrics.torproject.org/

## Ideas

 - Recreate default metrics visualization: user stats, bridge stats.
 - Create new dashboards: user stats + bridge stats per country. (DONE)
 - Create alerts per country (bridges, directly connecting Tor users).
 - Add download CSV to the dashboards. See: https://www.metabase.com/docs/latest/administration-guide/13-embedding.html
 - Add bridge stats per transport and country.
 - Investigate other sources: OONI Explorer, ioda.caida.org,
 - Add to the dashboard how many internet shutdowns happened in that country in 2020.
 - Dashboard: User Stats, Bridges users, and relays per country.

## Todo

 - Write a cronjob to run script `metricsdownloader.sh` every day.
 - Move Metabase to a server.

## Questions

 - How Metabase alerts works? Is it possible to define a goal per country?

## Limitations

 - You can't add more than one question per time to the dashboard (i.e., you will need to add country by country manually): 
   https://github.com/metabase/metabase/issues/12809
 - Data model metrics: you need to add one by one, you can't create a SQL query to iterate and return all countries + SUM of users.
 -

## Other ideas

  - Hackweek - download [daily anomalies](http://lists.infolabe.net/archives/infolabe-anomalies/2021-April/001473.html), parse and import to a new dashboard. 
